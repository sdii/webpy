# README #

Django based website for the EFFECTs project.

A few examples (wordVisual and sdii) are also available. The 'sdii' example website is set up as like 'effects' but just uses a different naming scheme. NOTE: the example may not match the main development version as it is not upkept like the 'effects' version.