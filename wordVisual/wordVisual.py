# ------------------------------------||------------------------------------- #
#                                 wordVisual
#
# Authors: Benjamin T. Davis, NSF Graduate Research Fellow
#
# Date Created: 29 July 2014 
#
# Reads text data and generates a Graphviz display
#
# Inputs:
# ------------------------------------||------------------------------------- #


import pydot
import re
#from IPython.display import Image

# Setting up the custom error message
class ErrorMessage(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)

# Setting up the graph
graph = pydot.Dot(graph_type='digraph')   # ready graph; digraph indicates to use arrows, graph just draws lines

# Accessing the file and getting data
with open('wordData.txt', 'r') as file:
    for line in file:
        endArrow = [m.start() for m in re.finditer('=>', line)]
        
        if endArrow:
            for idx, loc2 in enumerate(endArrow):
                loc1 = line.rfind('=', 0, loc2)
                
    #            if loc1 == -1 or loc2 == -1:    # check for syntax
    #                raise ErrorMessage('Incorrect syntax. Use the form "nodename ==> nodename" or "nodename = arrowname => nodename"')
                
                if idx == 0:                   
                    fromNode = line[:loc1]              # grab begin node label
    
                else:
                    fromNode = line[endArrow[idx-1]+2:loc1]      # grab begin node label
                
                if loc2 == endArrow[-1]:
                    toNode = line[loc2+2:]              # grab end node label
                        
                else:
                    loc3 = line.find('=', loc2+1)
                    toNode = line[loc2+2:loc3]        # grab end node label            
    
                edgeLabel = line[loc1+1:loc2]         # grab connector label
                
    #            print 'fromNode: ',fromNode            
    #            print 'toNode: ',toNode
    #            print 'label: ',edgeLabel
                            
                fromNode = fromNode.strip(' \t\n\r')    # removes all white space and new line characters
                edgeLabel = edgeLabel.strip(' \t\n\r')  
                toNode = toNode.strip(' \t\n\r')
                
                node1 = pydot.Node(fromNode)        # creates the "from" node, or gets one already made
                graph.add_node(node1)               # adds the node to the graph if needed
                
                node2 = pydot.Node(toNode)          # creates the "to" node
                graph.add_node(node2)
                
                edge = pydot.Edge(node1, node2, label=' '+edgeLabel, fontsize="14")     # create the connecting edge
                graph.add_edge(edge)
    
graph.write_gif('example-concept-map.png', prog='dot') # print to file

#png_str = graph.create_png()   # create postscript string representing image
#test = Image(data=png_str)     # create an image object for IPython console to display


