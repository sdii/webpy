# ------------------------------------||------------------------------------- #
#                               effects/urls.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 September 23
#
# Description:
#   The master url file that routes page requests to the specified locations
# ------------------------------------||--------------------------------------#

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'effects.views.home', name='home'),
    # url(r'^blog/', include('blog.urls', namespace='blog')),

    url(r'^$', 'home.views.homePage', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^journal/', include('journal.urls', namespace='journal')),
    url(r'^registration/', include('registration.urls', namespace='registration')),

    # DO NOT CHANGE THE FOLLOWING - these are for the operation of the built in authentication for Django
    # Taken from C:\Anaconda\Lib\site-packages\django\contrib\auth\urls.py
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^password_change/$', 'django.contrib.auth.views.password_change', name='password_change'),
    url(r'^password_change/done/$', 'django.contrib.auth.views.password_change_done', name='password_change_done'),
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),  
)
