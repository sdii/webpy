"""
Django settings for effects project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
with open(os.path.join(os.path.dirname(__file__), 'secret_key.txt')) as f:
    SECRET_KEY = f.read().strip()

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['oat.engr.sc.edu']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'journal',
    'home',
    'registration',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'effects.urls'

WSGI_APPLICATION = 'effects.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'effects_django',
        'USER': 'effects_web_user',
        'PASSWORD': '20illactyidf14',
        'HOST': '127.0.0.1',            # An IP Address that your DB is hosted on; for localhost use '127.0.0.1'
        'PORT': '3306',
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'effects.sqlite3'),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/New_York'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'staticroot')
STATIC_URL = '/static/'                                 # sets the typical directory to search for static files in; this tells Django to search 'myapp/static' in the site directory
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]   # sets additional directories to search for static files in
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]   # sets additional directories to search for template files in


# User
# https://docs.djangoproject.com/en/1.7/topics/auth/default/
LOGIN_URL = '/login/'


# Administrators - for exception error reporting; sends and email to the below people
# https://docs.djangoproject.com/en/1.7/howto/error-reporting/
ADMINS = (
    ('Juan Caicedo', 'caicedo@cec.sc.edu'),
    ('Benjamin Davis', 'btdavis@email.sc.edu'),
)

EMAIL_SUBJECT_PREFIX = '[EFFECTs Django] Error: '


# Managers - for broken link reporting; sends and email to the below people
# https://docs.djangoproject.com/en/1.7/howto/error-reporting/
SEND_BROKEN_LINK_EMAILS = True

MANAGERS = (
    ('Juan Caicedo', 'caicedo@cec.sc.edu'),
    ('Benjamin Davis', 'btdavis@email.sc.edu'),
)


# Email
# https://docs.djangoproject.com/en/1.7/topics/email/
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'effects.webmaster@gmail.com'
EMAIL_HOST_PASSWORD = 'timepii12cl'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend' # used for testing development code; turn off for deployment and uncomment the above code
#EMAIL_FILE_PATH = 'C:\\Users\\btdavis\\Desktop\\email-backend' # used for testing development code; locations to dump emails
DEFAULT_FROM_EMAIL = 'effects.webmaster@gmail.com'


# Log In redirect default - where the user is sent after logging in
LOGIN_REDIRECT_URL = '/' # sends user to the home page


# Cookies
#CSRF_COOKIE_SECURE = True
#SESSION_COOKIE_SECURE = True