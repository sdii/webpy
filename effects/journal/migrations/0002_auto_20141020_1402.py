# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='instructions',
            field=models.TextField(verbose_name=b'Instructions'),
        ),
        migrations.AlterField(
            model_name='journal',
            name='title',
            field=models.TextField(verbose_name=b'Title'),
        ),
        migrations.AlterField(
            model_name='journalentry',
            name='course',
            field=models.TextField(verbose_name=b'Course'),
        ),
        migrations.AlterField(
            model_name='journalentry',
            name='entry',
            field=models.TextField(verbose_name=b'Entry'),
        ),
        migrations.AlterField(
            model_name='journalentry',
            name='journal_type',
            field=models.TextField(default=b'Journal Entry', verbose_name=b'Journal Type', choices=[(b'Journal Entry', b'Journal Entry'), (b'Concept Map', b'Concept Map')]),
        ),
        migrations.AlterField(
            model_name='journalentry',
            name='topic',
            field=models.TextField(verbose_name=b'Topic'),
        ),
    ]
