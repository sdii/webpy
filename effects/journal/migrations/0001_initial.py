# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Journal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('instructions', models.TextField()),
                ('date', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Date Created')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JournalEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course', models.TextField()),
                ('topic', models.TextField()),
                ('journal_type', models.TextField(default=b'Journal Entry', choices=[(b'Journal Entry', b'Journal Entry'), (b'Concept Map', b'Concept Map')])),
                ('entry', models.TextField()),
                ('date_submitted', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Date Submitted')),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('journal', models.ForeignKey(to='journal.Journal')),
            ],
            options={
                'verbose_name_plural': 'Journal Entries',
            },
            bases=(models.Model,),
        ),
    ]
