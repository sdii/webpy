# ------------------------------------||------------------------------------- #
#                           registration/admin.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 September 11
#
# Description:
#   Controls the way the model (database entries) appears and functions in 
#       the admin site
# ------------------------------------||------------------------------------- #

from django.contrib import admin
from registration.models import Activation

class ActivationAdmin(admin.ModelAdmin):
    readonly_fields = ('user', 'activation_code')                                   # makes these fields uneditable
    list_display = ('person_name', 'date_joined', 'user', 'activation_code')        # controls which items are displayed and the order; can be a method callable from model class
    list_filter = ['user__date_joined']                                             # use __ to indicate a subfield of a foreign key 
    search_fields = ['user__first_name', 'user__last_name', 'user__date_joined']    # what fields to search in
    
admin.site.register(Activation, ActivationAdmin)            # tells Django to show the model Activation on the administration page

