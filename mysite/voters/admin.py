from django.contrib import admin
from voters.models import Voter
from voters.models import VChoice

#class ChoiceInline(admin.StackedInline):
class VoterChoiceInline(admin.TabularInline):
    model = VChoice
    extra = 3

class VoterAdmin(admin.ModelAdmin):
    list_display = ('question', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']    
    
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    
    inlines = [VoterChoiceInline]

# add a class for textentry?

admin.site.register(Voter, VoterAdmin)
