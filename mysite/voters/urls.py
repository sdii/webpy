from django.conf.urls import patterns, url
from voters import views

urlpatterns = patterns('',
#    # ex: /polls/
#    url(r'^$', views.index, name='index'),
#    # ex: /polls/specifics/5/
#    url(r'^specifics/(?P<poll_id>\d+)/$', views.detail, name='detail'),
#    # ex: /polls/5/results/
#    url(r'^(?P<poll_id>\d+)/results/$', views.results, name='results'),
#    # ex: /polls/5/vote/
#    url(r'^(?P<poll_id>\d+)/vote/$', views.vote, name='vote'),
    
    url(r'^$', views.VIndexView.as_view(), name='vindex'),
    url(r'^(?P<pk>\d+)/$', views.VDetailView.as_view(), name='vdetail'),
    url(r'^(?P<pk>\d+)/results/$', views.VResultsView.as_view(), name='vresults'),
    url(r'^(?P<vote_id>\d+)/vote/$', views.vvote, name='vvote'),
)

