import datetime
from django.utils import timezone
from django.db import models

class Voter(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    
    def __unicode__(self):
        return self.question
    
    def was_published_recently(self):
        #return self.pub_date >= timezone.now() - datetime.timedelta(days=1) # returns Boolean based on condition
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
        
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class VChoice(models.Model):
    voter = models.ForeignKey(Voter)
    vchoice_text = models.CharField(max_length=200)
    vvotes = models.IntegerField(default=0)
    
    def __unicode__(self):
        return self.vchoice_text
