from django.db import models

class TextInput(models.Model):
    value = models.TextField()
    
    def __unicode__(self):
        return self.value
