from django.conf.urls import patterns, url
from textvisual import views

urlpatterns = patterns('',
    # /textvisual/
    url(r'^$', views.input, name='input'),

    # /textvisual/results
    url(r'^textread/results/(?P<text>\w+)$', views.results, name='results'),

    # /textvisual/textRead
    url(r'^textRead/$', views.textRead, name='textRead'),
    
   # url(r'^$', views.IndexView.as_view(), name='index'),
)

