from django.contrib import admin
from textvisual.models import TextInput

class TextVisualAdmin(admin.ModelAdmin):
    fields = ['value']

admin.site.register(TextInput, TextVisualAdmin)
