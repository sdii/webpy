from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from textvisual.models import TextInput

def input(request):
    #return HttpResponse("Hello, world. You're at the text page - yay")
    value = get_object_or_404(TextInput)
    return render(request, 'textvisual/input.html', {'value': value})

def textRead(request):
    text = request.POST['value']
    return HttpResponseRedirect(reverse('textvisual:results', args=(text,)))
    #return HttpResponse(request.POST['value'])

def results(request, text):
    return HttpResponse('Your text was: %s' %text)
