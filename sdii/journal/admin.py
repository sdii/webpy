# ------------------------------------||------------------------------------- #
#                               journal/admin.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 August 03
#
# Description:
#   Controls the way the model (database entries) appears and functions in 
#       the admin site
# ------------------------------------||------------------------------------- #

from django.contrib import admin
from journal.models import Journal, JournalEntry

class JournalAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'was_published_recently')  # controls how the list of journals page is displayed
    list_filter = ['date']                                      # controls how the list of journals is filtered
    search_fields = ['title']                                   # controls what portion of the list is used for searches
    
    #field = ['title','instructions','date']        # controls the order and what fields of model Journal are shown
    fieldsets = [
        (None,                  {'fields': ['title','instructions']}),
        ('Date Information',    {'fields': ['date'], 'classes': ['collapse']}),
    ]                                               # controls the order, fields shown, groupings, etc for model when a specific entry is selected

class JournalEntryAdmin(admin.ModelAdmin):
    list_display = ('journal', 'course', 'person_name', 'date_submitted')
    list_filter = ['journal__title', 'course', 'date_submitted']                                # use __ to indicate a subfield of a foreign key
    search_fields = ['author__first_name', 'author__last_name', 'journal__title', 'course']

admin.site.register(Journal, JournalAdmin)              # tells Django to show the model Journal on the administration page
admin.site.register(JournalEntry, JournalEntryAdmin)    # tells Django to show the model JournalEntry on the administration page
