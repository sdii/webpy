# ------------------------------------||------------------------------------- #
#                               journal/views.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 August 03
# ------------------------------------||------------------------------------- #

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from journal.models import Journal, JournalEntry
import pydot

@login_required                                                                         # requires a site log in before access
def journalIndexPage(request):                                                          # page containing the index of available journals
    journals = Journal.objects.order_by('date')                                         # attempts to grab the Journal objects from database and sort ascending by date field ('-date' orders descending)
    
    if journals:                                                                        # if there are journals available
        author = User.objects.get(username=request.user.username)                       # get the user requesting the page
        journal_entry = []                                                              # initialize
        number = []
        i = 1
    
        for j in journals:                                                                  # work through each journal entry
            if JournalEntry.objects.filter(author=author).filter(journal=j).exists():       # if an entry exists for this journal
                journal_entry.append(JournalEntry.objects.get(journal=j, author=author))    # get the specific entry
            else:
                journal_entry.append('')                                                    # leave slot for that journal empty
            
            number.append(i)                                                                # save the journal number we are on
            i += 1                                                                          # increment
            
        zipped_var = zip(number, journals, journal_entry)                         # matches the indexes to create one giant iterable
  
    else:
        zipped_var = []
        
    return render(request, 'journal/journalIndexPage.html', {'zipped_var': zipped_var}) # uses the template 'journal/journalIndexPage.html' and passes zipped variables to template


@login_required                                                                         # requires a site log in before access
def journalEntryPage(request, journal_id):                                              # page containing the form for submitting a journal entry
    selectedJournal = get_object_or_404(Journal, pk=journal_id)                         # attempts to load the journal based on the id supplied
    return render(request,'journal/journalEntryPage.html', {'selectedJournal': selectedJournal})


@login_required                                                                         # requires a site log in before access
def journalUserPage(request):                                                           # page containing all the user journal submissions
    author = User.objects.get(username=request.user.username)
    user_journals = JournalEntry.objects.filter(author=author)
    user_entries = []    
    
    for uj in user_journals:
        if uj.journal_type == 'Journal Entry':
            user_entries.append('<br />'.join(uj.entry.split('\n')))            
            
        elif uj.journal_type == 'Concept Map':    
            begin_img = uj.entry.find('***')                                                # find the begining of the graphviz code for image
            end_img = uj.entry.find('***', begin_img+1) + 4                                 # find the end of the graphviz code for image; +4 puts the location after the '***'
            
            before_text = '<br />'.join(uj.entry[:begin_img].split('\n'))                   # do this to switch from text format to html format for rendering       
            after_text = '<br />'.join(uj.entry[end_img:].split('\n'))                      # do this to switch from text format to html format for rendering
            user_entries.append([before_text, after_text])           
        
    zvar = zip(user_journals, user_entries)                                             # matches indices for looping using Django in the template
    return render(request,'journal/journalUserPage.html', {'zvar': zvar, 'author': author})


@login_required                                                                         # requires a site log in before access 
def journalSubmitEntry(request, journal_id):                                            # form submission handler for journalEntryPage
    journal = Journal.objects.get(pk=journal_id)
    author = User.objects.get(username=request.user.username)
    
    if JournalEntry.objects.filter(author=author).filter(journal=journal).exists():     # send user to the update page since a journal entry has already been submitted
        return HttpResponseRedirect(reverse('journal:journalEntryUpdatePage', args=(journal_id,)))
    else:           
        je_obj = JournalEntry(journal=journal, 
                              author=author, 
                              course=request.POST['course'], 
                              topic=request.POST['topic'], 
                              journal_type=request.POST['journal_type'],
                              entry=request.POST['entry'].split('\n'))
        je_obj.save()    
    
    return HttpResponseRedirect(reverse('journal:journalUserPage'))


@login_required                                                                                                                 # requires a site log in before access 
def journalEntryUpdatePage(request, journal_id):                                                                                # page for updating a journal entry
    current_entry = JournalEntry.objects.get(journal__id = journal_id, author__username = request.user.username)                # get the current entry for the journal we are updating

    if current_entry.journal_type == 'Journal Entry':
        entry = '<br />'.join(current_entry.entry.split('\n'))
        
    elif current_entry.journal_type == 'Concept Map':    
        begin_img = current_entry.entry.find('***')                                     # find the begining of the graphviz code for image
        end_img = current_entry.entry.find('***', begin_img+1) + 4                      # find the end of the graphviz code for image; +4 puts the location after the '***'
            
        before_text = '<br />'.join(current_entry.entry[:begin_img].split('\n'))                   # do this to switch from text format to html format for rendering       
        after_text = '<br />'.join(current_entry.entry[end_img:].split('\n'))                      # do this to switch from text format to html format for rendering          

        entry = [before_text, after_text]

    return render(request,'journal/journalEntryUpdatePage.html', {'current_entry': current_entry, 'entry': entry})


@login_required                                                                                                                 # requires a site log in before access 
def journalSubmitEntryUpdate(request, journal_id):                                                                              # form submission handler for journalEntryUpdatePage
    je_obj = JournalEntry.objects.get(journal__id = journal_id, author__username = request.user.username)                       # get the associated entry for the journal
    
    je_obj.course = request.POST['course']                                              # update fields; ignore author and journal fields as these would not change
    je_obj.topic = request.POST['topic']
    je_obj.journal_type = request.POST['journal_type']
    je_obj.entry = request.POST['entry']

    je_obj.save()                                                                       # save updates
    
    return HttpResponseRedirect(reverse('journal:journalUserPage'))                     # send user to the overview page


def journalExampleConceptMapPage(request):
    return render(request, 'journal/journalExampleConceptMapPage.html')

def journalConceptMapImage(request, journal_id):
    journal_entry = JournalEntry.objects.get(journal__id = journal_id, author__username = request.user.username) # get journal entry we are working with
    
    begin_img = journal_entry.entry.find('***') + 4                 # find the tag that indicates the beginning of the figure; +4 moves the marker to after the ***
    end_img = journal_entry.entry.find('***', begin_img+1) - 1      # find the tag that indicates the end of the figure; -1 moves the marker to before the ***
    
    img_code = journal_entry.entry[begin_img:end_img]   # truncate the entry to just the image code
    
    row_ends = [0]                                      # prime the row_ends list
    loc = 0                                             # prime the end-of-line location
            
    while loc != -1:                                    # while the location of the end of the line (indicated by \n) is found -> (-1) means the string was not found
        loc = img_code.find('\n', loc+1)                # get the location of the next end-of-line
                
        if loc != -1:                                   # if end-of-line found
            row_ends.append(loc)                        # add to list of row_end locations
        
    if row_ends[-1] != len(img_code):                   # if the last entry is not the same as the length of the string, add a row_end indicating the end of the string
        row_ends.append(len(img_code))                  # prevents issues with missing the last line of input from the user
    
    graph = pydot.Dot(graph_type='digraph')             # ready graph; 'digraph' indicates to use arrows, 'graph' just draws lines   
            
    for i in range(1,len(row_ends)):
        line = img_code[row_ends[i-1]:row_ends[i]]  # a 'line' is the characters between end-of-line
        loc1 = line.find('=')                       # find first spacer
        loc2 = line.find('=>', loc1 + 1) + 1        # find second spacer
                    
        fromNode = line[:loc1-1]                    # grab begin node label
        edgeLabel = line[loc1+1:loc2-1]             # grab connector label
        toNode = line[loc2+1:]                      # grab end node label
                    
        fromNode = fromNode.strip(' \t\n\r')        # removes all white space and new line characters
        edgeLabel = edgeLabel.strip(' \t\n\r')
        toNode = toNode.strip(' \t\n\r')             
                    
        node1 = pydot.Node(fromNode)                # creates the "from" node, or gets one already made
        graph.add_node(node1)                       # adds the node to the graph if needed
                
        node2 = pydot.Node(toNode)                  # creates the "to" node
        graph.add_node(node2)
                
        edge = pydot.Edge(node1, node2, label=' '+edgeLabel, fontsize="14")     # create the connecting edge
        graph.add_edge(edge)    

    #graph.write_jpg('visual.jpg', prog='dot')      # saves the image as visual.jpg in the currenty directory
    png_str = graph.create_png()                    # makes a png postscript string

    return HttpResponse(png_str, content_type="image/png")

# reverse() can pass an args=(variable,) argument to fill the next view function's variable inputs (from django.core.urlresolvers)
# HttpResponse() can return an output of text or HTML without using a template (from django.http)