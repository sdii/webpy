# ------------------------------------||------------------------------------- #
#                               journal/urls.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 August 03
# ------------------------------------||------------------------------------- #

from django.conf.urls import patterns, url
from journal import views

urlpatterns = patterns('',
    # /journal/                   
    url(r'^$', views.journalIndexPage, name='journalIndexPage'),

    # /journal/<journal_id>/ ---> the (?P<journal_id>\d+) tells Django to read a number from the url pass it as journal_id to the view
    url(r'^(?P<journal_id>\d+)/$', views.journalEntryPage, name='journalEntryPage'),

    # /journal/<journal_id>/update ---> url for updating a specific journal entry
    url(r'^(?P<journal_id>\d+)/update/$', views.journalEntryUpdatePage, name='journalEntryUpdatePage'),

    # /journal/<journal_id>/submit/ ---> handles the form submittal from journalEntryPage
    url(r'^(?P<journal_id>\d+)/submit/$', views.journalSubmitEntry, name='journalSubmitEntry'),

    # /journal/<journal_id>/update/submit/ ---> a place holder for a function that handles the form submittal from journalEntryPage
    url(r'^(?P<journal_id>\d+)/update/submit/$', views.journalSubmitEntryUpdate, name='journalSubmitEntryUpdate'),

    # /journal/all/ ---> url for page that displays all entries for a user
    url(r'^all/$', views.journalUserPage, name='journalUserPage'),

    # /journal/<journal_id>/concept-map-image/ ---> link for the concept map png image
    url(r'^(?P<journal_id>\d+)/concept-map-image/$', views.journalConceptMapImage, name='journalConceptMapImage'),

    # /journal/example/concept-map/ ---> displays the example concept map input and output
    url(r'^example/concept-map/$', views.journalExampleConceptMapPage, name='journalExampleConceptMapPage'),
)

# (?P<username>\w+)     ---> w+ indicates a string
# (?P<journal_id>\d+)   ---> d+ indicates a number