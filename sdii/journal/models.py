# ------------------------------------||------------------------------------- #
#                               journal/models.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 August 03
# ------------------------------------||------------------------------------- #

import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User

# contains information on the journal for the student assignment
class Journal(models.Model):
    title = models.TextField('Title')                                           # the name in '' will the be verbose name for the field
    instructions = models.TextField('Instructions')
    date = models.DateTimeField('Date Created', default=datetime.datetime.now)
    
    def __unicode__(self):                                                      # defines what is returned when the model is called directly
        return self.title
    
    def was_published_recently(self):                                           # defines a method that can be accesed in the site admin
        now = timezone.now()
        return now - datetime.timedelta(days=7) <= self.date <= now             # returns a boolean if the date of the Journal is in the range
        
    was_published_recently.admin_order_field = 'date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published Recently?'

# contains the actual student entries for the journal
class JournalEntry(models.Model):
    JOURNAL_TYPE_CHOICES = (
        ('Journal Entry', 'Journal Entry'),
        ('Concept Map', 'Concept Map'),    
    )                                           # first entry is value to be set on the model; second entry is the display name
    
    journal = models.ForeignKey(Journal)
    author = models.ForeignKey(User)    
    course = models.TextField('Course')
    topic = models.TextField('Topic')  
    journal_type = models.TextField('Journal Type', choices=JOURNAL_TYPE_CHOICES, default=JOURNAL_TYPE_CHOICES[0][0])
    entry = models.TextField('Entry')
    date_submitted = models.DateTimeField('Date Submitted', default=datetime.datetime.now)
        
    def __unicode__(self):                                                      # defines what is returned when the model is called directly
        return self.journal.title
    
    def person_name(self):                                                      # defines a method for getting the user's name from the model
        return self.author.first_name + ' ' + self.author.last_name
    
    person_name.short_description = 'Author'
    
    class Meta:                                                                 # defines options for the model itself
        verbose_name_plural = 'Journal Entries';                                # sets the plural human readable name
