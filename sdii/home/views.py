# ------------------------------------||------------------------------------- #
#                               home/views.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 August 03
# ------------------------------------||------------------------------------- #

from django.shortcuts import render, render_to_response
from django.template import RequestContext
 
def homePage(request):
    return render_to_response('home/homePage.html', context_instance=RequestContext(request))
