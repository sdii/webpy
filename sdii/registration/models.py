# ------------------------------------||------------------------------------- #
#                           registration/models.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 September 09
#
# Description:
#   Defines the database models and features for activating user accounts
# ------------------------------------||------------------------------------- #

from django.db import models
from django.contrib.auth.models import User

# contains the activation code for a new user
class Activation(models.Model):
    user = models.ForeignKey(User)
    activation_code = models.TextField()
    
    user.verbose_name = 'Username'                                      # sets the human-readable name for the db field; Django uses this for display   
    activation_code.verbose_name = 'Activation Code'                    # sets the human-readable name for the db field; Django uses this for display
    
    def __unicode__(self):                                              # defines what is returned when the model is called directly
        return self.activation_code
    
    def person_name(self):                                              # defines a method that can be accesed in the site admin
        return self.user.first_name + ' ' + self.user.last_name
    
    person_name.short_description = 'Name'                              # sets the human-readable name for the field; Django uses this for display
    
    def date_joined(self):                                              # defines a method that can be accesed in the site admin
        return self.user.date_joined                                    # this is the same as the data the person registered
    
    date_joined.short_description = 'Date Joined'                       # sets the short description for the field; Django uses this for display
  
