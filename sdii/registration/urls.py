# ------------------------------------||------------------------------------- #
#                            registration/urls.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 September 09
# ------------------------------------||------------------------------------- #

from django.conf.urls import patterns, url
from registration import views

urlpatterns = patterns('',
    # /registration/                
    url(r'^$', views.registrationPage, name='registrationPage'),                       
                       
    # /registration/submit                  
    url(r'^submit/$', views.registrationSubmit, name='registrationSubmit'),

    # /registration/activation/resend
    url(r'^activation/resend/$', views.activationResendPage, name='activationResendPage'),

    # /registration/activation/resend
    url(r'^activation/resend/submit/$', views.activationResendSubmit, name='activationResendSubmit'),

    # registration/activation/                  
    url(r'^activation/(?P<code>\w+)$', views.activationPage, name='activationPage'),

    # registration/username/recover
    url(r'^username/recover/$', views.forgotUsernamePage, name='forgotUsernamePage'),

    # registration/username/recover/submit
    url(r'^username/recover/submit/$', views.forgotUsernameSubmit, name='forgotUsernameSubmit'),
)

