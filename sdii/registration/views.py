# ------------------------------------||------------------------------------- #
#                           registration/views.py
#
# Authors: 
#   Benjamin T. Davis
#   NSF Graduate Research Fellow
#   Structural Dynamics and Intelligent Infrastructure Research Group (SDII)
#   University of South Carolina
#   http://sdii.ce.sc.edu/
#
# Date Created: 2014 September 06
#
# Description:
#   Extends the functionality of the website based on django.contrib.admin for
#       site user accounts to include registration, activation, username
#       recovery
# ------------------------------------||------------------------------------- #

from django.shortcuts import render
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.template.loader import get_template
from django.template import Context
from django.conf import settings
from django.core.urlresolvers import reverse

from registration.models import Activation

# view for the registration form
def registrationPage(request):                                      
    return render(request, 'registration/registrationPage.html')    # uses the template 'registration/registrationPage.html'

# submit handler that validates the registration form and generates the completed registration view
def registrationSubmit(request):
    error_message = list()
    entered_fields = {'first_name': request.POST['first_name'],
                      'last_name': request.POST['last_name'],
                      'email': request.POST['email'],
                      'username': request.POST['username'],
                     }
       
    # checking for errors
    if User.objects.filter(username=request.POST['username']).exists():
        error_message.append('Username <i>' + request.POST['username'] + '</i> already exists. Click <a href="' + reverse('registration:activationResendPage') + '">here</a> to resend the activation')
        entered_fields['username'] = ''

    if request.POST['password1'] != request.POST['password2']:
        error_message.append('The passwords do not match.')
    
    if len(request.POST['password1']) < 8:
        error_message.append('The password must be at least 8 characters long.')
    
    # if there are any errors, report them and show the form for the user to adjust
    if error_message:
        return render(request, 'registration/registrationPage.html', {'error_message':error_message, 'entered_fields':entered_fields})    # go back to the registration form
       
    # if we get to this point, all checks have passed and user does not currently exist
    user_obj = User.objects.create_user(first_name = request.POST['first_name'], 
                    last_name = request.POST['last_name'],                    
                    username = request.POST['username'],
                    password = request.POST['password1'])
                    
    user_obj.email = User.objects.normalize_email(request.POST['email'])    # makes domain all lowercase     
    user_obj.is_active = False                                              # this is how we control log in, set to True once activated
    user_obj.save()                                                         # save new user to database
    
    # generate new activation code and url
    act_obj = Activation(user = User.objects.get(username = request.POST['username']),
                         activation_code = User.objects.make_random_password())
    act_obj.save()                                                          # save in database
    
    # generate the email from templates    
    plain_text = get_template('registration/activationEmail.txt')           # get the templates for the email
    html = get_template('registration/activationEmail.html')
    subject = get_template('registration/activationEmailSubject.txt')
    
    var = Context({'first_name': request.POST['first_name'],    # create the contect dictionary for filling in the templates
                   'last_name': request.POST['last_name'], 
                   'username': request.POST['username'], 
                   'site_name': request.get_host(),             #TODO get a global name set for the website. Maybe in settings.py?
                   'activation_link': request.get_host() + reverse('registration:activationPage', args=(act_obj.activation_code,))})
    
    plain_text_content = plain_text.render(var)                 # render the templates using the context dictionary
    html_content = html.render(var)
    subject_content = subject.render(var)
    
    send_mail(subject = subject_content,                        # send the email
              message = plain_text_content, 
              from_email = settings.DEFAULT_FROM_EMAIL,         # this value is set in the conf setting.py file for the website
              recipient_list = [user_obj.email],                # can add multiple emails in a list for multiple "to" field receipients
              html_message = html_content)    
     
    return render(request, 'registration/registrationSubmitPage.html', {'email': user_obj.email})   # go to the registration done page

# handler for activating the user and display success or failure
def activationPage(request, code):
    if Activation.objects.filter(activation_code = code).exists():      # if this is a valid activation link
        activation = Activation.objects.get(activation_code = code)     # grab the activation entry from database
        user = User.objects.get(username = activation.user.username)    # use activation link to pull up the correct user from database
        
        user.is_active = True   # activate the user on the website
        user.save()             # save the updated user to database
        
        activation.delete()     # delete activation entry from database as user is now activated
        
        return render(request, 'registration/activationPage.html', {'username': user.username})   # go to the activation page
       
    else:                       # not a valid activation link
        return render(request, 'registration/activationPage.html', {'error_message': 'Sorry! This is not a valid activation link.'})   # go to the activation page with an error

# the activation resend page
def activationResendPage(request):
    return render(request, 'registration/activationResendPage.html')

# handler for resending activation codes
def activationResendSubmit(request):
    if User.objects.filter(username = request.POST['username']).exists():   # checking if the user even exists
        user = User.objects.get(username = request.POST['username'])        # pull up the correct user from database 
        
        if user.is_active == True:      # if the user is active already, display to the requester
            return render(request, 'registration/activationResendPage.html', {'error_message': 'The provided username is already activated.', 'username': user.username})
        
        else:                           # the user is not active so check for an available activation code
            if Activation.objects.filter(user__username = user.username).exists():   # user has an activation code                 
                activation = Activation.objects.get(user = user)                                # grab the activation entry from database
                
                plain_text = get_template('registration/activationEmail.txt')                   # get the templates for the email
                html = get_template('registration/activationEmail.html')
                subject = get_template('registration/activationEmailSubject.txt')
                
                var = Context({'first_name': user.first_name,               # create the contect dictionary for filling in the templates
                               'last_name': user.last_name, 
                               'username': user.username, 
                               'site_name': request.get_host(),             #TODO get a global name set for the website. Maybe in settings.py?
                               'activation_link': request.get_host() + reverse('registration:activationPage', args=(activation.activation_code,))})
                
                plain_text_content = plain_text.render(var)                 # render the templates using the context dictionary
                html_content = html.render(var)
                subject_content = subject.render(var)
                
                send_mail(subject = subject_content,                        # send the email
                          message = plain_text_content, 
                          from_email = settings.DEFAULT_FROM_EMAIL,         # this value is set in the conf setting.py file for the website
                          recipient_list = [user.email],                    # can add multiple emails in a list for multiple "to" field receipients
                          html_message = html_content)            
                          
                return render(request, 'registration/activationResendSubmitPage.html', {'email': user.email})   # go to the activation page
                
            else:   # user exists but is not active and does not have an activation code
                return render(request, 'registration/activationResendPage.html', {'error_message': 'The provided user is not active nor has an activation code. Contact the site team to correct this issue.', 'username': user.username})
            
    else:   # user does not exist
        return render(request, 'registration/activationResendPage.html', {'error_message': 'The provided username does not exist. Please <a href="' + reverse('registration:registrationPage') + '">register</a> the new user account or <a href="' + reverse('registration:forgotUsernamePage') + '">recover</a> your username.', 'username': request.POST['username']})

# forgotten username form
def forgotUsernamePage(request):
    return render(request, 'registration/forgotUsernamePage.html')

# handler for checking the email for username recovery
def forgotUsernameSubmit(request):
    if User.objects.filter(email = request.POST['email']).exists():
        user = User.objects.get(email = request.POST['email'])        # pull up the correct user from database 
        
        plain_text = get_template('registration/forgotUsernameEmail.txt')                   # get the templates for the email
        html = get_template('registration/forgotUsernameEmail.html')
        subject = get_template('registration/forgotUsernameEmailSubject.txt')
                
        var = Context({'first_name': user.first_name,               # create the contect dictionary for filling in the templates
                       'last_name': user.last_name, 
                       'username': user.username, 
                       'site_name': request.get_host()})            #TODO get a global name set for the website. Maybe in settings.py?                      
                
        plain_text_content = plain_text.render(var)                 # render the templates using the context dictionary
        html_content = html.render(var)
        subject_content = subject.render(var)
                
        send_mail(subject = subject_content,                        # send the email
                  message = plain_text_content, 
                  from_email = settings.DEFAULT_FROM_EMAIL,         # this value is set in the conf setting.py file for the website
                  recipient_list = [user.email],                    # can add multiple emails in a list for multiple "to" field receipients
                  html_message = html_content)                    
        
        return render(request, 'registration/forgotUsernameSubmitPage.html', {'email': request.POST['email']})
    else:
        return render(request, 'registration/forgotUsernamePage.html', {'error_message': 'The provided email does not match any user accounts. Please <a href="' + reverse('registration:registrationPage') + '">register</a> the new user account.', 'email': request.POST['email']})